# Fondamentaux IA

## 1️⃣ ~ **21-10-2019**

> Chaine youtube => [Two Minutes Papers](https://www.youtube.com/user/keeroyz)

Ancienne vision de l'IA => résolution d'algorithme (ex: *le plus court chemin*).

#### Définition
    Ensemble des théories et techniques qui confèrent à des machines la capacité d'accomplir des tâches attribuées aux être intelligents.

#### Disciplines

- IA Générale

    > Regroupement de tous les domaines en un.

    > Domaine possiblement **inatteignable**. Beaucoup de contraintes techniques & matériels.

- Apprentissage automatique (*apprentissage par l'exemple* **[supervisé / non-supervisé]**, ex: *traduction de textes*)
  - Apprentissage par renforcement 
  
    > Agent artificiel effectue un action dans son environnement & un superviseur juge cette action (Bien / Mauvais).

    > *AlphaGo => Apprentissage supervisé.*
  
  - Apprentissage profond (**Deep Learning**)

    > AlphaZero

- Communication / Perception / Action
  - Traitement du langage naturel (ex: *Siri* , *Cortana*, etc)
  - Vision par ordinateur
  - Robotique
- Connaissance / Raisonnement / Plannification
- Résolution de problèmes

    > Le plus court chemin (*Drijksta / A\* / AGreedy / NBA\* / D\**)

- Informatique cognitive

    > Modélisation des intéractions humaines (ex: *emotions, intéractions*).

    > Domaine Pluridisciplinaire : psychologie, etc.

    > Sous domaine de l'IA, chargé de comprendre et simuler les processus cognitifs humains dans le but d'améliorer l'intéraction homme-machine.

- Systèmes multi-agents

    > Intéraction d'un grand nombre d'agents (ex: *simulations de foule*)

#### Domaines d'applications

- Traitement du langage naturel
- Systèmes prédictifs
- Reconnaissance d'images / de vidéos
  - Colorisation d'images
  - Amélioration de photos
  - Transfert de style
  - Identification des objets d'une scène visuelle
  - Description d'une scène visuelle
  - Détection de pose
  - Capture de mouvements
  - Synthèse de sons
- Systèmes de recommandation
- Aide à la décision
- Robotique
- Optimisation

#### Exercice ▶️ jusqu'à la fin du module [Fondamentaux de l'IA]

    Réaliser une cartographie des applications de l'IA.

#### Histoire de l'IA

- Naissance ▶️ Années 1950-1956

    > Idée / But: *Simuler le fonctionnement d'un cerveau humain*. Ambitions surréalistes. Création d'une machine pleinement intelligente dans les 20 prochaines années.

- Age d'or ▶️ Années 1956-1974

    > Premier programme capable de jouer aux echecs (1952), apprentissage par renforcement (joue contre lui même). Premier robot industriel (**Unimate**, 1961). Premier robot mobile *Shakey*. Premier chatbot (simulation d'une discution avec un thérapeute).

- 1er Hiver ▶️ Années 1974-1980

    > Désintérêt de la part des chercheurs. Moins de financement. Rencontre de plusieurs difficultés notamment dans la traduction. Découverte des limites des buts annoncées durant la Naissance de l'IA ▶️ Remise en question.

- 2e Age d'or ▶️ Années 1980-1987

    > Re-nouveau de l'IA. Création du système expert (ex: catégoriser & synthétiser les connaissances d'un ou plusieurs médecins afin de réaliser un diagnostic). Nouvelles applications, théories, etc.

- 2e Hiver ▶️ Années 1987-1993

    > Nombreuses déceptions causées par des attentes non réalisés/réalisables.

- Maturité discrète ▶️ Années 1993-2011
- Essor du Deep Learning ▶️ Années 2011-Aujd

## 2️⃣ ~ **22-10-2019**